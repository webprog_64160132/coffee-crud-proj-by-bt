export default interface Order {
  id: number;
  name: string;
  price: number;
  count: number;
  sum: number;
}
