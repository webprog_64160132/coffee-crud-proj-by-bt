import { ref } from "vue";
import { defineStore } from "pinia";
import type Coffee from "@/types/Coffee";

export const useCoffeeStore = defineStore("coffee", () => {
  const coffee = ref<Coffee[]>([
    {
      id: 1,
      name: "coffee",
      price: 35,
      category: "drink",
      img: "https://images.immediate.co.uk/production/volatile/sites/30/2020/08/flat-white-3402c4f.jpg",
    },
    {
      id: 2,
      name: "milk",
      price: 35,
      category: "drink",
      img: "https://images.immediate.co.uk/production/volatile/sites/30/2020/02/Glass-and-bottle-of-milk-fe0997a.jpg",
    },
    {
      id: 3,
      name: "ไข่กระทะ",
      price: 35,
      category: "food",
      img: "https://img.wongnai.com/p/1920x0/2017/09/26/41d1915ec9e34ec0bedf262465752d99.jpg",
    },
    {
      id: 4,
      name: "ครัวซองต์",
      price: 35,
      category: "dessert",
      img: "https://s359.kapook.com/pagebuilder/61452a62-9a93-496f-b2c4-02c34a885d40.jpg",
    },
  ]);
  const name = (id: number) => {
    return coffee.value[id].name;
  };
  const price = (id: number) => {
    return coffee.value[id].price;
  };
  const Id = (id: number) => {
    return coffee.value[id].id;
  };

  return { coffee, name, price, Id };
});
