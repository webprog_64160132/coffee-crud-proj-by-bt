import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeOut = ref(2000);
  const showMsg = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeOut.value = tout;
  };
  const closeMsg = () => {
    message.value = "msg";
    isShow.value = false;
  };
  return { isShow, message, showMsg, closeMsg, timeOut };
});
