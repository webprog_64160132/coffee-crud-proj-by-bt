import type Order from "@/types/Order";
import type Coffee from "@/types/Coffee";
import { defineStore } from "pinia";
import { ref } from "vue";
import { useCoffeeStore } from "./coffee";
const useCoffee = useCoffeeStore();
export const useOrderStore = defineStore("order", () => {
  const order = ref<Order[]>([]);
  const OnClilk = (id: number) => {
    if (order.value.findIndex((item) => item.id == id) === -1) {
      order.value.push({
        id: useCoffee.Id(id - 1),
        name: useCoffee.name(id - 1),
        price: useCoffee.price(id - 1),
        count: 1,
        sum: useCoffee.price(id - 1),
      });
    } else {
      const index = order.value.findIndex((item) => item.id == id);
      order.value[index].count++;
    }
  };
  return { OnClilk, order };
});
